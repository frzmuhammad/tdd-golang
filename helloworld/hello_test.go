package main

import "testing"

func TestHello(t *testing.T) {
	assertCorrectMessage := func(t testing.TB, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("saying hellow to people", func(t *testing.T) {
		got := Hello("fariz", "English")
		want := "Hellow, fariz"

		assertCorrectMessage(t, got, want)
	})

	t.Run("say hello world when empty string is supplied", func(t *testing.T) {
		got := Hello("", "")
		want := "Hellow, World"

		assertCorrectMessage(t, got, want)
	})

	t.Run("say in spanish", func(t *testing.T) {
		got := Hello("Elodie", "Spanish")
		want := "Hola, Elodie"

		assertCorrectMessage(t, got, want)
	})

	t.Run("say in French", func(t *testing.T) {
		got := Hello("Worldy", "French")
		want := "Bonjour, Worldy"

		assertCorrectMessage(t, got, want)
	})
}
