package iteration

func Repeat(words string, repeat int) string {
	var repeated string
	for i := 0; i < repeat; i++ {
		repeated += words
	}

	return repeated
}
