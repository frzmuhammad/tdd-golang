package pointers

import "errors"

var ErrInsufficientFunds = errors.New("cannot withdraw, insufficient funds")

type Crypto int

type Wallet struct {
	balance Crypto
}

func (w *Wallet) Deposit(amount Crypto) {
	w.balance += amount
}

func (w *Wallet) Balance() Crypto {
	return w.balance
}

func (w *Wallet) Withdraw(amount Crypto) error {
	if amount > w.balance {
		return ErrInsufficientFunds
	}

	w.balance -= amount
	return nil
}
