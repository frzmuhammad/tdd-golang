package pointers

import "testing"

func TestWallet(t *testing.T) {

	t.Run("Deposit", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Crypto(10))
		assertBalance(t, wallet, Crypto(10))
	})

	t.Run("Withdraw with funds", func(t *testing.T) {
		wallet := Wallet{Crypto(20)}
		err := wallet.Withdraw(Crypto(10))
		assertNoError(t, err)
		assertBalance(t, wallet, Crypto(10))
	})

	t.Run("Withdraw insufficient funds", func(t *testing.T) {
		wallet := Wallet{Crypto(20)}
		err := wallet.Withdraw(Crypto(100))

		assertError(t, err, ErrInsufficientFunds)
		assertBalance(t, wallet, Crypto(20))
	})
}

func assertBalance(t testing.TB, wallet Wallet, want Crypto) {
	t.Helper()
	got := wallet.Balance()

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}

func assertError(t testing.TB, got, want error) {
	t.Helper()
	if got == nil {
		t.Fatal("didn't get an error but wanted one")
	}

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}

func assertNoError(t testing.TB, got error) {
	t.Helper()
	if got != nil {
		t.Fatal("got an error but didn't want one")
	}
}
