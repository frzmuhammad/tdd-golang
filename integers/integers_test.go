package integers

import "testing"

func TestSummary(t *testing.T) {
	assertCorrectAnswer := func(t testing.TB, sum, want int) {
		t.Helper()
		if sum != want {
			t.Errorf("want %d but sum %d", want, sum)
		}
	}

	t.Run("adding number", func(t *testing.T) {
		sum := Sum(2, 2, "add")
		want := 4
		assertCorrectAnswer(t, sum, want)
	})

	t.Run("subtract sum", func(t *testing.T) {
		sum := Sum(4, 2, "subtract")
		want := 2
		assertCorrectAnswer(t, sum, want)
	})

	t.Run("multiply sum", func(t *testing.T) {
		sum := Sum(4, 2, "multiply")
		want := 8
		assertCorrectAnswer(t, sum, want)
	})

	t.Run("divide sum", func(t *testing.T) {
		sum := Sum(4, 2, "divide")
		want := 2
		assertCorrectAnswer(t, sum, want)
	})
}
