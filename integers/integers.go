package integers

const add = "add"
const subtract = "subtract"
const multiply = "multiply"
const divide = "divide"

func Sum(x, y int, types string) (sum int) {
	switch types {
	case add:
		sum = Add(x, y)
	case subtract:
		sum = Sub(x, y)
	case multiply:
		sum = Multiply(x, y)
	case divide:
		sum = Divide(x, y)
	}
	return
}

func Add(x, y int) int {
	return x + y
}

func Sub(x, y int) int {
	return x - y
}

func Multiply(x, y int) int {
	return x * y
}

func Divide(x, y int) int {
	return x / y
}
